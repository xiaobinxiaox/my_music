import request from '../../utils/request'

// pages/video/video.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 导航标签数据
    videoGroupList: [],
    // 导航标识
    navId: '',
    // 视频列表数据
    videoList: [],
    // 视频的id标识
    videoId: '',
    // 记录每个video播放时长的数组
    videoUpdateTime: [],
    // 标识下拉刷新是否被触发
    isTriggered: false,
    // 上拉刷新页面标识
    offset:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取导航标签数据
    this.getVideoGroupListData();
    // 获取视频列表数据,放在这里onload只执行一次，如果第一次没有navId就不再发请求了，所以不能放在这里
    // this.getVideoList(this.data.navId);
  },

  // 获取导航标签数据函数
  async getVideoGroupListData () {
    let videoGroupListData = await request('/video/group/list');
    // 截取前14个标签并更新到data
    this.setData({
      videoGroupList: videoGroupListData.data.slice(0, 14),
      navId:videoGroupListData.data[0].id
    })
    // console.log(videoGroupListData);
    // console.log(this.data.videoGroupList);

    // 获取视频列表数据
    this.getVideoList(this.data.navId);
  },

  // 点击切换导航标识
  changeNav (event) {
    // 通过id向event传参时如果传的是num类型会自动转换成字符串类型
    // let navId = event.currentTarget.id;
    // 通过data-key形式传参如果传的是num类型还是num类型
    let navId = event.currentTarget.dataset.id;
    this.setData({
      // 通过id向event传，要转换成同类型才能比较
      // navId:navId*1
      // 通过data-key传，不用转换，还是原来num类型
      navId,
      // 切换的时候把之前的视频列表数据置空，页面就会空白
      videoList: [],
      // 上拉标识也置0
      offset:0
    })

    // 显示正在加载提示框
    wx.showLoading({
      title:'正在加载'
    })

    // 动态获取当前导航对应的视频列表数据
    this.getVideoList(this.data.navId);
  },

 /********************************************* 
  // 获取视频列表数据（内网穿透api）
  async getVideoList (navId) {
    // // 先判断navId是否有值，再发请求
    // if (!navId) {
    //   return;
    // }

    let videoListData = await request('/video/group', { id: navId });
    console.log(videoListData);
    // 给视频列表数据添加id属性，用于wx:key
    let index = 0;
    let videoList = videoListData.datas.map(item => {
      item.id = index++;
      return item;
    })
    console.log(videoListData);
    this.setData({
      // videoList:videoListData.datas
      videoList
    })
    console.log(videoList);
  },
  **********************************************/


  // 获取视频列表数据（vercel部署api）
  async getVideoList (navId) {
    // // 先判断navId是否有值，再发请求
    // if (!navId) {
    //   return;
    // }

    let videoListData = await request('/video/group', { id: navId });
    // console.log(videoListData, '00000');
    
    // let videoList = videoListData.datas;
    // for (let index = 0; index < videoList.length; index++){
    //   videoList[index].id = index;
    //   let vidUrlData = await request('/video/url', { id: videoList[index].data.vid });
    //   videoList[index].vidUrl = vidUrlData.urls[0].url;
    // }

    // 给视频列表数据添加id属性，用于wx:key。当然也可以使用data.vid。
    let index = 0;
    let videoList =await Promise.all(videoListData.datas.map(async (item) => {
        item.id = index++;
        // 获取视频播放地址
        let vidUrlData = await request('/video/url', { id: item.data.vid });
        // 插入视频播放地址
        item.vidUrl = vidUrlData.urls[0].url;
        return item;
      }))

    // 关闭“正在加载”提示框
    wx.hideLoading();
    
    
    this.setData({
      // videoList:videoListData.datas
      videoList,
      // 关闭下拉刷新
      isTriggered:false
    })
    // console.log(videoList,'1111');
  },


 
  // 视频开始播放/继续播放处理函数
  handlePlay (event) {
    // 使用image代替video之后，就不用再考虑如何关闭上一个视频了。

    // // 1.获取当前点击的video的id
    // let vid = event.currentTarget.id;
    /* 2.关闭上一个播放的视频:
        2.1.1 第一次进来时，当前页面（this）身上没有vid属性，所以不等于vid成立，继续执行下去；
        2.1.2 第一次进来时，当前页面（this）身上也没有videoContext属性,则为空不执行后面&&语句，不会停止视频播放；
        2.2.1 第二次进来时已经有了vid、videoContext属性，则先比较vid，若相同则不执行&&后面的语句，不停止播放视频；（即点击同一个视频）
        2.2.2 第二次进来时如果this.id与当前vid不一样，则继续执行，因为this.videoContext不为空，所以执行后面stop()关闭上一个视频；（即点击不同视频关闭上一个视频）
        2.3 后面每一次进来都先比较vid，一样则不停止播放，不一样则停止上一个视频播放。（此时this.videoContext还是指向上一个实例）
    */
    // this.vid !== vid && this.videoContext && this.videoContext.stop();
    // 3.当前点击的vid赋值给this.vid
    // this.vid = vid;

    // // 更新data中的videoId
    // this.setData({
    //   videoId:vid
    // })

    let { videoId, videoUpdateTime } = this.data;
    this.videoContext = wx.createVideoContext(videoId);
    let videoItem = videoUpdateTime.find(item => item.vid === videoId);

    // 4.创建控制video标签的实例对象，并且让this.videoContext指向当前实例对象。
    // this.videoContext = wx.createVideoContext(vid);

    
    // // 从data中取出记录播放时长数组
    // let { videoUpdateTime } = this.data;
    // // 根据id查询是否有播放记录，有记录就返回（指向）符合条件的该对象
    // let videoItem = videoUpdateTime.find(item => item.vid === vid);
    
    // 判断当前视频是否有播放记录
    if (videoItem) {
      // 如果有播放记录，调用api跳转到记录的时长
      this.videoContext.seek(videoItem.currentTime);
      
    } 
    
    // 如果没有播放记录就从0秒自动播放

    // 播放视频(或者在video标签添加autoplay属性也可以)
    // this.videoContext.play();

  },

  // 点击图片获取视频id赋值给页面显示video标签
  handleImagePlay (event) {
    let vid = event.currentTarget.id;
    // 更新data中的videoId
    this.setData({
      videoId:vid
    })
    
  },
  

  // 监听每个视频播放进度的函数
  // 视频播放过程中会不断触发这个函数，在event对象里面可以拿到当前的播放时间等信息。
  handleTimeUpdate (event) {
    // console.log(event);
    // 记录当前视频id和播放的时长
    let videoTimeObj = {
      vid: event.currentTarget.id,
      currentTime: event.detail.currentTime
    };
    // 获取data中记录每个视频播放时长的数组对象
    let { videoUpdateTime } = this.data;
    /* 
      1. find() 方法返回通过测试（函数内判断为true）的数组的第一个元素的值；
      2. 查找记录播放时长的 videoUpdateTime 数组中是否有当前视频的播放记录（即vid）；
      3. 拿videoUpdateTime数组每一个对象的id与当前播放视频id做比较，如果有则返回该对象。
      */
    let videoItem = videoUpdateTime.find(item => item.vid === videoTimeObj.vid);
    // 
    if (videoItem) {
      // 如果有则把当前新的播放时长赋值给原记录的数组对象
      videoItem.currentTime = event.detail.currentTime;
    } else {
      // 如果没有则在记录时长的数组中push当前视频id和时长进去
      videoUpdateTime.push(videoTimeObj);
    }
    
    // 更新data中的videoUpdateTime数组
    this.setData({
      videoUpdateTime
    })
  },

  // 视频播放结束时的处理函数
  handleEnded (event) {
    // console.log(event);
    // 移除data中记录当前视频播放时长的数组对象
    let { videoUpdateTime } = this.data;
    videoUpdateTime.splice(videoUpdateTime.findIndex(item => item.vid === event.currentTarget.id), 1);

    // 更新data中的 videoUpdateTime 数组
    this.setData({
      videoUpdateTime
    })
  },

  // 自定义视频列表下拉刷新函数
  handleRefresher () {
    // console.log('下拉刷新');
    // 发送请求，获取最新的视频列表数据
    this.getVideoList(this.data.navId);
  },

  // 自定义上拉触底处理函数
  async handleToLower () {
    // 显示正在加载提示框
    wx.showLoading({
      title:'正在加载'
    })

    // 发送请求，在前端截取最新的数据，追加到视频列表后方
    // 模拟拿到的最新数据
/******* 
    let newVideoList = [
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_A1AA69DDCF03B301571952F7303E75FF",
          "coverUrl": "https://p2.music.126.net/sGzhJRMlLQZkZviVoqzrQw==/109951163572660255.jpg",
          "height": 540,
          "width": 960,
          "title": "评委以为她是智障，结果拉小提琴的一刻震惊了！",
          "description": "评委以为她是智障，结果拉小提琴的一刻震惊了！",
          "commentCount": 6012,
          "shareCount": 10128,
          "resolutions": [
            {
              "resolution": 240,
              "size": 28684241
            },
            {
              "resolution": 480,
              "size": 40978683
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 350000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "http://p1.music.126.net/ij7Ju0b3AjLPA5wWjjYd9g==/109951163092932590.jpg",
            "accountStatus": 0,
            "gender": 1,
            "city": 350200,
            "birthday": 654192000000,
            "userId": 31925677,
            "userType": 200,
            "nickname": "半泽树",
            "signature": "Beatlemania / 云村活跃分子 / 坏品味 / 60~70's",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951163092932590,
            "backgroundImgId": 109951163039740560,
            "backgroundUrl": "http://p1.music.126.net/obV7H1zUaj9kDPyK45kHjQ==/109951163039740567.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": [
              "影视原声",
              "欧美"
            ],
            "experts": {
              "1": "泛生活视频达人",
              "2": "生活图文达人"
            },
            "djStatus": 10,
            "vipType": 11,
            "remarkName": null,
            "backgroundImgIdStr": "109951163039740567",
            "avatarImgIdStr": "109951163092932590"
          },
          "urlInfo": null,
          "videoGroup": [
            {
              "id": 58100,
              "name": "现场",
              "alg": null
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": null
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": null
            },
            {
              "id": 4101,
              "name": "娱乐",
              "alg": null
            },
            {
              "id": 3101,
              "name": "综艺",
              "alg": null
            },
            {
              "id": 14201,
              "name": "小提琴",
              "alg": null
            }
          ],
          "previewUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/preview_21727684_HldfnROo.webp?wsSecret=a5d6b8ee8ffa6cf542a43471b9a17756&wsTime=1636471549",
          "previewDurationms": 4000,
          "hasRelatedGameAd": false,
          "markTypes": [
            109
          ],
          "relateSong": [],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "A1AA69DDCF03B301571952F7303E75FF",
          "durationms": 241792,
          "playTime": 9360498,
          "praisedCount": 80966,
          "praised": false,
          "subscribed": false
        },
        "id": 0,
        "vidUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/E7FevvUh_21727684_hd.mp4?ts=1636475150&rid=859AEE2C48CF2D760780ED0AD0C6ED2A&rl=3&rs=QAVCmoMLOEPhYCceXEekCCXSCgYouBTb&sign=d5ce73b93f27173c67744e44db36266e&ext=3Pwq4tpFfMYnifra1W4bF7jXSdTQS77%2BNQ0dd2cUQTWBqL%2BU8iCH%2F1nkxBpbmw8dDGeh3v340gH4h6eMPLdTSzQmWC%2BKmcGRbWZ7ca43mNo%2FVSVu9j%2FxQ7oPO%2FkVetn0U5FtQvpR9udMU%2FRTgcOMw7Ul4KxGS3voRMqQDaSHHkbDNOf0MApjCgslfCWo60v5WH6hMlfPVOpuhopV7AqbnDF%2BucH%2Bqx2jV3lBpcZcpjG801MgCF6qHxAp2MWzi3Fj"
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_588C08D47D087140107A259E7EDCFDDE",
          "coverUrl": "https://p1.music.126.net/yzTJZw_Emg8qvXnybkI66w==/109951163573072995.jpg",
          "height": 720,
          "width": 1280,
          "title": "歌神和唱后更动听,学友一开口,我吓到了,这不就是CD嘛",
          "description": "经典歌曲，歌神和唱后更动听,学友一开口,我吓到了,这不就是CD嘛",
          "commentCount": 1710,
          "shareCount": 4435,
          "resolutions": [
            {
              "resolution": 240,
              "size": 38447720
            },
            {
              "resolution": 480,
              "size": 54734677
            },
            {
              "resolution": 720,
              "size": 87998538
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 230000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "http://p1.music.126.net/RtLEWx9-qM9O6jzHXDVYzA==/109951166033998302.jpg",
            "accountStatus": 0,
            "gender": 1,
            "city": 230100,
            "birthday": 733766400000,
            "userId": 631445975,
            "userType": 0,
            "nickname": "姐夫剪刀手",
            "signature": "音乐是一种符号，声音符号，表达人的所思所想。音乐是有目的的，是有内涵的，它可以带给人美的享受和表达人的情感。音乐是社会行为的一种形式，通过音乐人们可以互相交流情感和生活体验。结交音乐知心朋友，让生活生动美好，音与美的结合过程是我的快乐追随。",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951166033998300,
            "backgroundImgId": 109951164261049820,
            "backgroundUrl": "http://p1.music.126.net/Ess_X3vSYAQ5cvm66wKxmQ==/109951164261049820.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": {
              "1": "音乐视频达人"
            },
            "djStatus": 0,
            "vipType": 11,
            "remarkName": null,
            "backgroundImgIdStr": "109951164261049820",
            "avatarImgIdStr": "109951166033998302"
          },
          "urlInfo": null,
          "videoGroup": [
            {
              "id": 58100,
              "name": "现场",
              "alg": null
            },
            {
              "id": 57105,
              "name": "粤语现场",
              "alg": null
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": null
            },
            {
              "id": 12100,
              "name": "流行",
              "alg": null
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": null
            },
            {
              "id": 15133,
              "name": "安静",
              "alg": null
            },
            {
              "id": 15103,
              "name": "张学友",
              "alg": null
            }
          ],
          "previewUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/preview_1333870358_HWPnm0DT.webp?wsSecret=6c08ff3358352e6a00bb66911fab1957&wsTime=1636471549",
          "previewDurationms": 4000,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [
            {
              "name": "谁可改变",
              "id": 152266,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 5205,
                  "name": "谭咏麟",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [
                "电视剧《天师执位》主题曲"
              ],
              "pop": 100,
              "st": 0,
              "rt": "600902000005375470",
              "fee": 8,
              "v": 14,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 15328,
                "name": "歌者恋歌浓情30年经典金曲",
                "picUrl": "http://p4.music.126.net/0g5XXOn-4BeBv2J4m-wXpw==/661905999933308.jpg",
                "tns": [],
                "pic": 661905999933308
              },
              "dt": 234760,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 9392631,
                "vd": 22141
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 5635596,
                "vd": 24774
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 3757079,
                "vd": 26541
              },
              "a": null,
              "cd": "1",
              "no": 3,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 1,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 7003,
              "mv": 0,
              "publishTime": 1151683200000,
              "privilege": {
                "id": 152266,
                "fee": 8,
                "payed": 0,
                "st": 0,
                "pl": 128000,
                "dl": 0,
                "sp": 7,
                "cp": 1,
                "subp": 1,
                "cs": false,
                "maxbr": 999000,
                "fl": 128000,
                "toast": false,
                "flag": 4,
                "preSell": false
              }
            }
          ],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "588C08D47D087140107A259E7EDCFDDE",
          "durationms": 328283,
          "playTime": 5262197,
          "praisedCount": 20857,
          "praised": false,
          "subscribed": false
        },
        "id": 1,
        "vidUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/Xr0R3rZh_1333870358_shd.mp4?ts=1636475151&rid=859AEE2C48CF2D760780ED0AD0C6ED2A&rl=3&rs=NuvNoCJCkXiyMZrNnaHynQraahkbCVwv&sign=9f0d57da3d8cb91e3f872e0b88beace4&ext=3Pwq4tpFfMYnifra1W4bF7jXSdTQS77%2BNQ0dd2cUQTUgJFdb5hJpv8XSvZyKo%2BuBeqgyjlp9A%2F5yawXzyyy12VGzo6Glu%2Fz91hY6Y2uBwQMaeoPQ9lb5kZqxKso1Qkkx3H3YRNOguF0rRNhCkg0xfVj0rBRqOsC2DljoGXpgIPDlo58qeOf2Yd8vEduJ1UtwPPQ1r4Zbh%2F%2B9K%2FJtFzVkmAS8L4JVHRxSTsvQR%2FFJ%2BUM3Gs%2Bs%2FPVUaBjg7V4iIXmu"
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_C59CE657068AC393952A9EA5043BE993",
          "coverUrl": "https://p2.music.126.net/ajcDwY_yjEnywMDFbM-15g==/109951163850151423.jpg",
          "height": 1080,
          "width": 1920,
          "title": "酷玩乐队Coldplay 全英音乐现场 - Hymn For The Weekend",
          "description": "全英音乐开场秀酷玩乐队#Coldplay#表演热单- Hymn for the Weekend,是酷玩乐队和美国歌手碧昂丝演唱的歌曲,由酷玩乐队填词谱曲,发行于2016年1月25日.该首歌曲被收录在酷玩乐队第七张录音室专辑《A Head Full Of Dreams》中.",
          "commentCount": 48,
          "shareCount": 277,
          "resolutions": [
            {
              "resolution": 240,
              "size": 49964551
            },
            {
              "resolution": 480,
              "size": 87824243
            },
            {
              "resolution": 720,
              "size": 132849280
            },
            {
              "resolution": 1080,
              "size": 204745128
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 1000000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "http://p1.music.126.net/vUMDn0fg2PGvt1RNN--bdw==/109951164195226542.jpg",
            "accountStatus": 0,
            "gender": 1,
            "city": 1010000,
            "birthday": 842716800000,
            "userId": 1329747020,
            "userType": 204,
            "nickname": "罗伊佐",
            "signature": "Welcome back to my channel,How are ya!",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951164195226540,
            "backgroundImgId": 109951165430502450,
            "backgroundUrl": "http://p1.music.126.net/0iCQUuyLWkMOdI5-LCfqXw==/109951165430502447.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": {
              "1": "音乐视频达人"
            },
            "djStatus": 10,
            "vipType": 0,
            "remarkName": null,
            "backgroundImgIdStr": "109951165430502447",
            "avatarImgIdStr": "109951164195226542"
          },
          "urlInfo": null,
          "videoGroup": [
            {
              "id": 58100,
              "name": "现场",
              "alg": null
            },
            {
              "id": 9102,
              "name": "演唱会",
              "alg": null
            },
            {
              "id": 57106,
              "name": "欧美现场",
              "alg": null
            },
            {
              "id": 57108,
              "name": "流行现场",
              "alg": null
            },
            {
              "id": 12125,
              "name": "Coldplay",
              "alg": null
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": null
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": null
            }
          ],
          "previewUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/preview_2305941198_WsVyK255.webp?wsSecret=8a161ff8bbf8aa82ab58615174661b16&wsTime=1636471549",
          "previewDurationms": 4000,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [
            {
              "name": "Hymn For The Weekend [Remix]",
              "id": 419594258,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 1045123,
                  "name": "Alan Walker",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 89365,
                  "name": "Coldplay",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [],
              "pop": 100,
              "st": 0,
              "rt": null,
              "fee": 8,
              "v": 107,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 34767073,
                "name": "Hymn For The Weekend [Remix]",
                "picUrl": "http://p3.music.126.net/zx0EgLRhfOo-q906272T0Q==/3427177756044412.jpg",
                "tns": [],
                "pic": 3427177756044412
              },
              "dt": 230373,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 9217089,
                "vd": -30100
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 5530271,
                "vd": -27600
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 3686862,
                "vd": -26500
              },
              "a": null,
              "cd": "1",
              "no": 1,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 0,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 14002,
              "mv": 5342021,
              "publishTime": 1467302400007,
              "privilege": {
                "id": 419594258,
                "fee": 8,
                "payed": 0,
                "st": 0,
                "pl": 128000,
                "dl": 0,
                "sp": 7,
                "cp": 1,
                "subp": 1,
                "cs": false,
                "maxbr": 320000,
                "fl": 128000,
                "toast": false,
                "flag": 256,
                "preSell": false
              }
            }
          ],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "C59CE657068AC393952A9EA5043BE993",
          "durationms": 235380,
          "playTime": 260538,
          "praisedCount": 1568,
          "praised": false,
          "subscribed": false
        },
        "id": 2,
        "vidUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/zSp96Lmu_2305941198_uhd.mp4?ts=1636475151&rid=859AEE2C48CF2D760780ED0AD0C6ED2A&rl=3&rs=rmXrXuCejxLwWMSrnchsLHCaBAkcUCIS&sign=7246d2a6ea20599b9f014796533ffe49&ext=3Pwq4tpFfMYnifra1W4bF7jXSdTQS77%2BNQ0dd2cUQTVA3z%2BxfXuGP12iayk5E%2FjcgxndzFA6fvB1hAwjQtWLjPhnmJbR4jMJ6cxydH2yni0Ozb4tY9R2L4DffASlVuom6QJ3Q9tja2Me15bL5qRMxKEhv8XUT9ZHg3g87esYr04sSAfwYy2tLvrmZdiTxzu9ra8iLF%2FXFd7UjHDT%2BaxEdbtHjd9IgCLWr6XL89Mqke4Rz3gU65kXQJ6PGtQfH8uH"
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_43A4D91D2B517F186B8269057C507905",
          "coverUrl": "https://p1.music.126.net/_nxi6tR4oWeMuK3-uunBGg==/109951163651776589.jpg",
          "height": 1080,
          "width": 1920,
          "title": "林彦俊 - 巡演It's OK换装混剪Focus",
          "description": "Dear Evan-林彦俊个站联合十二献上的FM巡演混剪。",
          "commentCount": 945,
          "shareCount": 4470,
          "resolutions": [
            {
              "resolution": 240,
              "size": 40828370
            },
            {
              "resolution": 480,
              "size": 89335972
            },
            {
              "resolution": 720,
              "size": 156833703
            },
            {
              "resolution": 1080,
              "size": 333430149
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 510000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "http://p1.music.126.net/zX5XjwKdQeP1mE6Pj7trOQ==/109951165013895322.jpg",
            "accountStatus": 0,
            "gender": 2,
            "city": 510100,
            "birthday": 727286400000,
            "userId": 1366136679,
            "userType": 0,
            "nickname": "意外著迷",
            "signature": "我已见过银河 却仍只钟情于你这一个星星",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951165013895330,
            "backgroundImgId": 109951164990708060,
            "backgroundUrl": "http://p1.music.126.net/c6gJSlHW3_JWwwxrTz1Lnw==/109951164990708058.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": {
              "1": "舞蹈原创视频达人"
            },
            "djStatus": 10,
            "vipType": 11,
            "remarkName": null,
            "backgroundImgIdStr": "109951164990708058",
            "avatarImgIdStr": "109951165013895322"
          },
          "urlInfo": null,
          "videoGroup": [
            {
              "id": 58100,
              "name": "现场",
              "alg": null
            },
            {
              "id": 59101,
              "name": "华语现场",
              "alg": null
            },
            {
              "id": 57108,
              "name": "流行现场",
              "alg": null
            },
            {
              "id": 57110,
              "name": "饭拍现场",
              "alg": null
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": null
            },
            {
              "id": 12100,
              "name": "流行",
              "alg": null
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": null
            },
            {
              "id": 13222,
              "name": "华语",
              "alg": null
            },
            {
              "id": 14137,
              "name": "感动",
              "alg": null
            }
          ],
          "previewUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/preview_1915373284_OpvWz0zJ.webp?wsSecret=dbc094f9dd9211decd3152d261e62f8a&wsTime=1636471549",
          "previewDurationms": 4000,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [
            {
              "name": "It's Ok (Live)",
              "id": 550136151,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 12054456,
                  "name": "董又霖",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 12493781,
                  "name": "Lil Ghost小鬼",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13056454,
                  "name": "灵超",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13057497,
                  "name": "黄明昊 (Justin)",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13057511,
                  "name": "郑艺彬",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13057676,
                  "name": "李希侃",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13058487,
                  "name": "毕雯珺",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13058498,
                  "name": "林彦俊",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13058503,
                  "name": "尤长靖",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13059447,
                  "name": "林超泽",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [],
              "pop": 100,
              "st": 0,
              "rt": null,
              "fee": 0,
              "v": 17,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 37579301,
                "name": "偶像练习生 表演曲目合集",
                "picUrl": "http://p3.music.126.net/0fNqjjWb3srRgcsb0w7Qzg==/109951163170276615.jpg",
                "tns": [],
                "pic_str": "109951163170276615",
                "pic": 109951163170276600
              },
              "dt": 197961,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 7921415,
                "vd": -24200
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 4752866,
                "vd": -21700
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 3168592,
                "vd": -20100
              },
              "a": null,
              "cd": "01",
              "no": 1,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 2,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 1377826,
              "mv": 0,
              "publishTime": 1522944000007,
              "privilege": {
                "id": 550136151,
                "fee": 0,
                "payed": 0,
                "st": 0,
                "pl": 320000,
                "dl": 999000,
                "sp": 7,
                "cp": 1,
                "subp": 1,
                "cs": false,
                "maxbr": 999000,
                "fl": 320000,
                "toast": false,
                "flag": 0,
                "preSell": false
              }
            }
          ],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "43A4D91D2B517F186B8269057C507905",
          "durationms": 193240,
          "playTime": 1825026,
          "praisedCount": 28449,
          "praised": false,
          "subscribed": false
        },
        "id": 3,
        "vidUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/M28NyCXk_1915373284_uhd.mp4?ts=1636475151&rid=859AEE2C48CF2D760780ED0AD0C6ED2A&rl=3&rs=rVSUqRZEQclqVgEbfPDySRxsABKtABKX&sign=f1682da986e8f7bbda424e572d82e5e3&ext=3Pwq4tpFfMYnifra1W4bF7jXSdTQS77%2BNQ0dd2cUQTVoxk8vtDQK6Zqo53YNGFgpOIvfZ927LbT5WYVbB06%2FKMoDGiHcucqqilvUBpXWHjWjljD%2BskJgEJYlGTXWHOqaqoRhVORfzQJ%2B2LBwWVV56u0FAG4muc4USkm79%2FLdk%2F4hoN7WWoOsq52PZ40AvEkkxLQjd7jfx22trz4PUTZCXy6igYvlCH6LQI%2B5rQg5C5glhc0e5XWTXUltvQmeeW6C"
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_C9B922379B360F30DEB88131B839203D",
          "coverUrl": "https://p2.music.126.net/JT9XMx7ACGDtbyRL8vRw6g==/109951165044812787.jpg",
          "height": 1080,
          "width": 1920,
          "title": "活死人 2019 Cypher 巡演完整版 LIVE",
          "description": "活死人 2019 Cypher WokenDay 巡演完整字幕版 LIVE\n\n出场成员：隆力奇、成都小李PISSY、BUZZY、福克斯Fox、JarStick、龙崎LIUSAKI、小精灵、法老、喉结蜥蜴、雷迪GanstaLady\n\n禁止转载🈲️",
          "commentCount": 195,
          "shareCount": 700,
          "resolutions": [
            {
              "resolution": 240,
              "size": 101243950
            },
            {
              "resolution": 480,
              "size": 161888957
            },
            {
              "resolution": 720,
              "size": 224911893
            },
            {
              "resolution": 1080,
              "size": 372709040
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 610000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "http://p1.music.126.net/dmSONOuKjm6h2hJsJnZYrg==/109951164608528935.jpg",
            "accountStatus": 0,
            "gender": 2,
            "city": 610100,
            "birthday": 1577808000000,
            "userId": 1962762251,
            "userType": 204,
            "nickname": "IN_LIVE",
            "signature": "一个LIVE摄录制作团队，让更多的人爱上音乐和LIVE。谢谢您的关注呀～\n\n关于LIVE视频的一些说明：\n对于由于以前录制的素材在现场离音响很近会有爆音，为了综合观感所以会替换掉部分音轨用录音室版本；机位视角不好的时候会替换为其他演唱时的片段不一定完全能对得上口形。\n\n我们只是音乐的爱好者很难按照idol的演唱会级别去录制地下歌手的现场，我们只想尽自己的微薄力量让这个文化让更多人喜欢。来记录那些大家参与过或者错过了的LIVE碎片。如果有侵犯到艺人的权益请私信我进行稿件处理。\n\n最后感谢您对这个文化的喜爱。",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951164608528930,
            "backgroundImgId": 109951162868128400,
            "backgroundUrl": "http://p1.music.126.net/2zSNIqTcpHL2jIvU6hG0EA==/109951162868128395.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": {
              "1": "音乐视频达人"
            },
            "djStatus": 0,
            "vipType": 11,
            "remarkName": null,
            "backgroundImgIdStr": "109951162868128395",
            "avatarImgIdStr": "109951164608528935"
          },
          "urlInfo": null,
          "videoGroup": [
            {
              "id": 58100,
              "name": "现场",
              "alg": null
            },
            {
              "id": 59101,
              "name": "华语现场",
              "alg": null
            },
            {
              "id": 59108,
              "name": "巡演现场",
              "alg": null
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": null
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": null
            }
          ],
          "previewUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/preview_3003378453_vwIYTz77.webp?wsSecret=6f44f8b04df94cfc22e38859ee298910&wsTime=1636471549",
          "previewDurationms": 4000,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [
            {
              "name": "活死人2019Cypher",
              "id": 1405207289,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 12200652,
                  "name": "活死人",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 12065015,
                  "name": "隆历奇",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 12851112,
                  "name": "李毅杰PISSY",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 12094558,
                  "name": "Buzzy",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 13899303,
                  "name": "杨和苏KeyNG",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 12092433,
                  "name": "JarStick",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 1185047,
                  "name": "龙崎",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 12287129,
                  "name": "小精灵",
                  "tns": [],
                  "alias": []
                },
                {
                  "id": 865007,
                  "name": "法老",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [],
              "pop": 100,
              "st": 0,
              "rt": "",
              "fee": 8,
              "v": 9,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 82892435,
                "name": "活死人2019Cypher",
                "picUrl": "http://p3.music.126.net/uc_hRoxl0h3f-84V1lJjbQ==/109951164502490453.jpg",
                "tns": [],
                "pic_str": "109951164502490453",
                "pic": 109951164502490450
              },
              "dt": 702000,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 28082721,
                "vd": -64288
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 16849650,
                "vd": -61737
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 11233115,
                "vd": -60184
              },
              "a": null,
              "cd": "01",
              "no": 1,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 0,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 0,
              "mv": 0,
              "publishTime": 0,
              "privilege": {
                "id": 1405207289,
                "fee": 8,
                "payed": 0,
                "st": 0,
                "pl": 128000,
                "dl": 0,
                "sp": 7,
                "cp": 1,
                "subp": 1,
                "cs": false,
                "maxbr": 999000,
                "fl": 128000,
                "toast": false,
                "flag": 0,
                "preSell": false
              }
            }
          ],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "C9B922379B360F30DEB88131B839203D",
          "durationms": 700916,
          "playTime": 476077,
          "praisedCount": 8370,
          "praised": false,
          "subscribed": false
        },
        "id": 4,
        "vidUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/vm2E1DV9_3003378453_uhd.mp4?ts=1636475151&rid=859AEE2C48CF2D760780ED0AD0C6ED2A&rl=3&rs=JATMOQjVvAwhEijqnFRUNolyNZoDXMUq&sign=c2b264ab8b2d69d11b1c664503370b2c&ext=3Pwq4tpFfMYnifra1W4bF7jXSdTQS77%2BNQ0dd2cUQTVoxk8vtDQK6Zqo53YNGFgpIa2pgTB6TPcSp0rxqQWnbyMcau2o5ZrzbZzV7BZL32OGXTacKUzyikdajkXHc%2FiznPXbnUlw%2FTvQhLhp53B8RvNWsIowvfEn8wme5HrU%2FVufSEWDcMNn%2FK5jg8Id0XP7lQ5QaU%2FRiU8TTF8E4aJtA6FUL7nRjyn1tH%2FQ6n333B1tEPyJ%2F74qENELVTNg%2F%2BMn"
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_2790AD37EA76047DCD132D3FAF47FEC6",
          "coverUrl": "https://p1.music.126.net/WhjXTclclWq0ggvjogYGsw==/109951163814224509.jpg",
          "height": 720,
          "width": 1280,
          "title": "-一样的惊艳！！！没有蒙面的周深唱《身骑白马》",
          "description": "一样的惊艳！！！\n没有蒙面的周深唱《身骑白马》醉了醉了",
          "commentCount": 451,
          "shareCount": 933,
          "resolutions": [
            {
              "resolution": 240,
              "size": 26458337
            },
            {
              "resolution": 480,
              "size": 43479718
            },
            {
              "resolution": 720,
              "size": 67108717
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 110000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "http://p1.music.126.net/4jCjuy8MzsU5x9chEj6WzQ==/109951163536519595.jpg",
            "accountStatus": 0,
            "gender": 2,
            "city": 110101,
            "birthday": -2209017600000,
            "userId": 320880971,
            "userType": 207,
            "nickname": "音乐热点通",
            "signature": "十九线热点",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951163536519600,
            "backgroundImgId": 2002210674180198,
            "backgroundUrl": "http://p1.music.126.net/i0qi6mibX8gq2SaLF1bYbA==/2002210674180198.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": {
              "1": "视频达人(华语、音乐现场)",
              "2": "音乐|生活图文达人"
            },
            "djStatus": 0,
            "vipType": 0,
            "remarkName": null,
            "backgroundImgIdStr": "2002210674180198",
            "avatarImgIdStr": "109951163536519595"
          },
          "urlInfo": null,
          "videoGroup": [
            {
              "id": 58100,
              "name": "现场",
              "alg": null
            },
            {
              "id": 9102,
              "name": "演唱会",
              "alg": null
            },
            {
              "id": 59101,
              "name": "华语现场",
              "alg": null
            },
            {
              "id": 57108,
              "name": "流行现场",
              "alg": null
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": null
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": null
            },
            {
              "id": 13211,
              "name": "周深",
              "alg": null
            }
          ],
          "previewUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/preview_2273427683_J7gaQHaE.webp?wsSecret=dfd06633aa77e48963198014f33d5151&wsTime=1636471549",
          "previewDurationms": 4000,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [
            {
              "name": "身骑白马",
              "id": 306664,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 9940,
                  "name": "徐佳莹",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [],
              "pop": 100,
              "st": 0,
              "rt": "600902000007958081",
              "fee": 1,
              "v": 337,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 30452,
                "name": "LaLa首张创作专辑",
                "picUrl": "http://p3.music.126.net/jSLMqcE_Yq27rRKDNrRKcA==/109951163187407183.jpg",
                "tns": [],
                "pic_str": "109951163187407183",
                "pic": 109951163187407180
              },
              "dt": 313186,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 12530460,
                "vd": -43666
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 7518293,
                "vd": -41102
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 5012210,
                "vd": -39431
              },
              "a": null,
              "cd": "1",
              "no": 6,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 0,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 22036,
              "mv": 5324094,
              "publishTime": 1243526400000,
              "privilege": {
                "id": 306664,
                "fee": 1,
                "payed": 0,
                "st": 0,
                "pl": 0,
                "dl": 0,
                "sp": 0,
                "cp": 0,
                "subp": 0,
                "cs": false,
                "maxbr": 999000,
                "fl": 0,
                "toast": false,
                "flag": 260,
                "preSell": false
              }
            }
          ],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "2790AD37EA76047DCD132D3FAF47FEC6",
          "durationms": 256392,
          "playTime": 2180867,
          "praisedCount": 11829,
          "praised": false,
          "subscribed": false
        },
        "id": 5,
        "vidUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/y0r1Gg1Q_2273427683_shd.mp4?ts=1636475151&rid=859AEE2C48CF2D760780ED0AD0C6ED2A&rl=3&rs=qRlOVzigWdkFALzpmkWzRRNMNITCpuPE&sign=b3716a36d4c5522bc000b0ba5511224c&ext=3Pwq4tpFfMYnifra1W4bF7jXSdTQS77%2BNQ0dd2cUQTVoxk8vtDQK6Zqo53YNGFgp6R2%2Frdfc2hvz5OH5jJDJY634j8lvZedtE5YuozzoXHEcYqMg59FjUNJoeLrIc%2FjqmFLjptO%2Fx%2B6LimW15wXNbsnAhrhTCkR%2FMZGtihy3vtYhL%2FVvZx%2Bqv%2B2EMiligr6nmDc6smW3pApcFlkoR2hJxZsVBR2wtfZAOiTyOY7uxiRWUbjel6cDCEw5ZhF8U6cm"
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_BBA1D88EBD21EDA5E274B401EA8B231D",
          "coverUrl": "https://p2.music.126.net/ULlY8dt8eUHMl59nHu075A==/109951163574339721.jpg",
          "height": 720,
          "width": 1280,
          "title": "拥有特效的舞台！！T-ara - No.9",
          "description": "想知道是什么样的特效吗，就快点开看看吧！！",
          "commentCount": 123,
          "shareCount": 163,
          "resolutions": [
            {
              "resolution": 240,
              "size": 47047058
            },
            {
              "resolution": 480,
              "size": 86496806
            },
            {
              "resolution": 720,
              "size": 101856656
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 110000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "http://p1.music.126.net/M2gId22LRR0vpWWSDIQvdQ==/109951163458813116.jpg",
            "accountStatus": 0,
            "gender": 2,
            "city": 110101,
            "birthday": 916329600000,
            "userId": 1344968972,
            "userType": 0,
            "nickname": "小兔哦哦安利酱",
            "signature": "喜欢音乐，喜欢现场，也喜欢分享，准备好接受安利了吗~",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951163458813120,
            "backgroundImgId": 109951162868126480,
            "backgroundUrl": "http://p1.music.126.net/_f8R60U9mZ42sSNvdPn2sQ==/109951162868126486.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": {
              "1": "音乐视频达人"
            },
            "djStatus": 0,
            "vipType": 0,
            "remarkName": null,
            "backgroundImgIdStr": "109951162868126486",
            "avatarImgIdStr": "109951163458813116"
          },
          "urlInfo": null,
          "videoGroup": [
            {
              "id": 58100,
              "name": "现场",
              "alg": null
            },
            {
              "id": 57107,
              "name": "韩语现场",
              "alg": null
            },
            {
              "id": 57108,
              "name": "流行现场",
              "alg": null
            },
            {
              "id": 9127,
              "name": "T-ara",
              "alg": null
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": null
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": null
            }
          ],
          "previewUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/preview_1979802596_QUpH5ytH.webp?wsSecret=3d9ef51148b35ff0f8b1ce5ed476f792&wsTime=1636471549",
          "previewDurationms": 4000,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "BBA1D88EBD21EDA5E274B401EA8B231D",
          "durationms": 228066,
          "playTime": 241498,
          "praisedCount": 1222,
          "praised": false,
          "subscribed": false
        },
        "id": 6,
        "vidUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/ldk7BBRV_1979802596_shd.mp4?ts=1636475152&rid=859AEE2C48CF2D760780ED0AD0C6ED2A&rl=3&rs=YzcbCZNzcnTpahOXuhcsNyqxdgejFadH&sign=cfb7a15e9f03db7ad224e83b814867cc&ext=3Pwq4tpFfMYnifra1W4bF7jXSdTQS77%2BNQ0dd2cUQTWBqL%2BU8iCH%2F1nkxBpbmw8dDGeh3v340gH4h6eMPLdTSzQmWC%2BKmcGRbWZ7ca43mNo%2FVSVu9j%2FxQ7oPO%2FkVetn0U5FtQvpR9udMU%2FRTgcOMw565waTayVLo2bXyiIsjk%2Fg9%2BdYA1tKImLqiSguXbPvmC%2FLr%2B5XpwxuZxwm5LBxBiOZQmWq8jN6kEoyhJ%2Bwq9wG%2BRrYTFUg3zyWUNxy90V09"
      },
      {
        "type": 1,
        "displayed": false,
        "alg": "onlineHotGroup",
        "extAlg": null,
        "data": {
          "alg": "onlineHotGroup",
          "scm": "1.music-video-timeline.video_timeline.video.181017.-295043608",
          "threadId": "R_VI_62_56B4EC5ADB8BDDF604123ECC56B62CE8",
          "coverUrl": "https://p2.music.126.net/jgQFDQKzlyArAPHQqr86sw==/109951163593610589.jpg",
          "height": 1080,
          "width": 1920,
          "title": "PSY - GENTLEMAN，鸟叔的又一倾情力作，果然嗨！",
          "description": "鬼畜神曲 ，简直无比魔性…… \r\n",
          "commentCount": 25,
          "shareCount": 95,
          "resolutions": [
            {
              "resolution": 240,
              "size": 28307122
            },
            {
              "resolution": 480,
              "size": 48491631
            },
            {
              "resolution": 720,
              "size": 71996859
            },
            {
              "resolution": 1080,
              "size": 114838699
            }
          ],
          "creator": {
            "defaultAvatar": false,
            "province": 110000,
            "authStatus": 0,
            "followed": false,
            "avatarUrl": "http://p1.music.126.net/4pUJ0mHUvFc5FmZ6Zsltbw==/109951163117873448.jpg",
            "accountStatus": 0,
            "gender": 2,
            "city": 110101,
            "birthday": 832521600000,
            "userId": 1332581534,
            "userType": 0,
            "nickname": "可靠的桃子",
            "signature": "音乐现场视频，了解一下？",
            "description": "",
            "detailDescription": "",
            "avatarImgId": 109951163117873440,
            "backgroundImgId": 109951162868126480,
            "backgroundUrl": "http://p1.music.126.net/_f8R60U9mZ42sSNvdPn2sQ==/109951162868126486.jpg",
            "authority": 0,
            "mutual": false,
            "expertTags": null,
            "experts": {
              "1": "音乐视频达人"
            },
            "djStatus": 0,
            "vipType": 0,
            "remarkName": null,
            "backgroundImgIdStr": "109951162868126486",
            "avatarImgIdStr": "109951163117873448"
          },
          "urlInfo": null,
          "videoGroup": [
            {
              "id": 58100,
              "name": "现场",
              "alg": null
            },
            {
              "id": 57107,
              "name": "韩语现场",
              "alg": null
            },
            {
              "id": 57108,
              "name": "流行现场",
              "alg": null
            },
            {
              "id": 59108,
              "name": "巡演现场",
              "alg": null
            },
            {
              "id": 1100,
              "name": "音乐现场",
              "alg": null
            },
            {
              "id": 12100,
              "name": "流行",
              "alg": null
            },
            {
              "id": 5100,
              "name": "音乐",
              "alg": null
            },
            {
              "id": 14146,
              "name": "兴奋",
              "alg": null
            }
          ],
          "previewUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/preview_2030779332_4BagbmRe.webp?wsSecret=99f771713c5a48bbf7cf98a536eeeaa2&wsTime=1636471549",
          "previewDurationms": 4000,
          "hasRelatedGameAd": false,
          "markTypes": null,
          "relateSong": [
            {
              "name": "Gentleman",
              "id": 26173033,
              "pst": 0,
              "t": 0,
              "ar": [
                {
                  "id": 124492,
                  "name": "PSY",
                  "tns": [],
                  "alias": []
                }
              ],
              "alia": [],
              "pop": 100,
              "st": 0,
              "rt": "600902000007946973",
              "fee": 1,
              "v": 31,
              "crbt": null,
              "cf": "",
              "al": {
                "id": 2408002,
                "name": "싸이-Gentleman",
                "picUrl": "http://p4.music.126.net/dJM7oH38TN59sV3H_fP_Yw==/2491493348565232.jpg",
                "tns": [],
                "pic": 2491493348565232
              },
              "dt": 194079,
              "h": {
                "br": 320000,
                "fid": 0,
                "size": 7765725,
                "vd": -46000
              },
              "m": {
                "br": 192000,
                "fid": 0,
                "size": 4659453,
                "vd": -43500
              },
              "l": {
                "br": 128000,
                "fid": 0,
                "size": 3106316,
                "vd": -42100
              },
              "a": null,
              "cd": "1",
              "no": 1,
              "rtUrl": null,
              "ftype": 0,
              "rtUrls": [],
              "djId": 0,
              "copyright": 1,
              "s_id": 0,
              "rtype": 0,
              "rurl": null,
              "mst": 9,
              "cp": 7003,
              "mv": 0,
              "publishTime": 1365696000007,
              "tns": [
                "绅士"
              ],
              "privilege": {
                "id": 26173033,
                "fee": 1,
                "payed": 0,
                "st": 0,
                "pl": 0,
                "dl": 0,
                "sp": 0,
                "cp": 0,
                "subp": 0,
                "cs": false,
                "maxbr": 999000,
                "fl": 0,
                "toast": false,
                "flag": 4,
                "preSell": false
              }
            }
          ],
          "relatedInfo": null,
          "videoUserLiveInfo": null,
          "vid": "56B4EC5ADB8BDDF604123ECC56B62CE8",
          "durationms": 192533,
          "playTime": 251782,
          "praisedCount": 653,
          "praised": false,
          "subscribed": false
        },
        "id": 7,
        "vidUrl": "http://vodkgeyttp9.vod.126.net/vodkgeyttp8/GPtMQbiP_2030779332_uhd.mp4?ts=1636475152&rid=859AEE2C48CF2D760780ED0AD0C6ED2A&rl=3&rs=vbHtlqpJAHdiqckVXNVStREPUQaiEzWd&sign=53d24afa996192614caaf66466c29fc8&ext=3Pwq4tpFfMYnifra1W4bF7jXSdTQS77%2BNQ0dd2cUQTVoxk8vtDQK6Zqo53YNGFgpOIvfZ927LbT5WYVbB06%2FKMoDGiHcucqqilvUBpXWHjWjljD%2BskJgEJYlGTXWHOqaqoRhVORfzQJ%2B2LBwWVV56iiNBRdY39kndQkxxcL5EITIdSyvqwAwDko1syAECmSM9n93gNQ8vTk2vRYlYHalcIIth91U4u5Z32nvTXSfl%2F9%2BcZLKefKkISZnIwFCXkcE"
      }
    ];
******************/

    // 每次上拉请求分页参数加1
    let offset = this.data.offset + 1;
    // 请求分页对应的视频列表数据
    let videoListData = await request('/video/group', { id: this.data.navId, offset: offset });
    // 遍历视频列表数据插入视频地址
    let newVideoList = await Promise.all(videoListData.datas.map(async (item) => {
      // 给视频列表数据添加id属性，用于wx:key。当然也可以使用data.vid。
      item.id = item.data.vid;
      // 获取视频播放地址
      let vidUrlData = await request('/video/url', { id: item.data.vid });
      // 插入视频播放地址
      item.vidUrl = vidUrlData.urls[0].url;
      return item;
    }))


    // 获取data中的视频列表数组
    let videoList = this.data.videoList;
    // 把新获取视频列表数组拆开push进去
    videoList.push(...newVideoList);
    // 更新data
    this.setData({
      videoList,
      offset
    })
    // console.log(offset);

    // 关闭“正在加载”提示框
    wx.hideLoading();

  },

  // 点击跳转到搜索页面
  toSearch () {
    wx.navigateTo({
      url:'/pages/search/search'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享/页面设置了转发的button时触发
   */
  onShareAppMessage: function (event) {
    // console.log(event);
    // 函数返回一个 Object，用于自定义转发内容
    // return {
    //   title: '自定义转发的标题',
    //   // 转发的页面路径
    //   page: '/pages/video/video',
    //   // 自定义图片路径
    //   imageUrl:'/static/images/my_jpg/002.jpg'  
    // }
    if (event.from === 'button') {
      return {
        title: '来自button的转发',
        // 转发的页面路径
        page: '/pages/video/video',
        // 自定义图片路径
        imageUrl:'/static/images/my_jpg/002.jpg'  
      }
    } else {
      return {
        title: '自定义右上角的转发',
        // 转发的页面路径
        page: '/pages/video/video',
        // 自定义图片路径
        imageUrl:'/static/images/my_jpg/001.jpg'  
      }
    }
  }

})