let startY = 0; //手指起始纵坐标
let moveY = 0;//手指移动后的纵坐标
let moveDistance = 0;//手指纵向移动距离
import request from '../../utils/request'

// pages/personal/personal.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 用户信息
    userInfo: {},
    // 用户播放记录
    recentPlayList:[],
    // 利用css3盒子模型的位移控制盒子的移动
    coverTransform: 'translateY(0)',
    coverTransition:''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 读取用户的基本信息(同步)
    let userInfo = wx.getStorageSync('userInfo');
    // 先判断是否有用户信息
    if (userInfo) {
      // 更新data中userInfo
      this.setData({
        userInfo:JSON.parse(userInfo)
      })

      // 获取用户的播放记录（在判断是否有用户信息里面，没有用户信息就不用获取）
      this.getUserRecentPlayList(this.data.userInfo.userId)
    }
  },

  // 获取用户播放记录函数
  async getUserRecentPlayList (userId) {
    // 根据id获取用户播放记录
    let recentPlayListData = await request('/user/record', { uid: userId, type: 0 })
    // 设置index作为id的值
    let index = 0;
    // 从用户播放记录里面截取前十首，并且通过map给每一首的对象添加id属性，方便最为循环时的wx:key
    let recentPlayList = recentPlayListData.allData.splice(0, 10).map(item => {
      item.id = index++;
      return item;
    });
    // 改造后的数组数据更新到data
    this.setData({
      recentPlayList
    })
  },

  // 手指触摸时处理函数
  handleTouchStart (event) {
    // this.setData({
    //   // 结束时才加回弹过度效果，触摸开始时把过度效果去掉
    //   coverTransition:''
    // })
    // console.log('start');
    startY = event.touches[0].clientY;//获取第一个手指起始的纵坐标
  },

  // 手指移动时处理函数
  handleTouchMove (event) {
    // console.log('move');
    moveY = event.touches[0].clientY;//获取第一个手指移动后的纵坐标
    moveDistance = moveY - startY;//手指纵向移动距离
    // console.log(moveDistance);

    // 判断移动距离，向上移动禁止，向下移动设置最大移动80rpx
    if (moveDistance <= 0) {
      return;
    }
    if (moveDistance >= 80) {
      moveDistance = 80;
    }
    // 动态更新coverTransform的值
    this.setData({
      coverTransform:`translateY(${moveDistance}rpx)`
    })
  },
  // 手指松开时处理函数
  handleTouchEnd () {
    // console.log('end');
    // 动态更新coverTransform的值
    this.setData({
      coverTransform: `translateY(0rpx)`,
      // 结束时才加回弹过度效果，触摸开始时把过度效果去掉
      // coverTransition:'transform 0.8s linear'
    })
  },

  // 点击头像跳转到登录界面处理函数
  toLogin () {
    wx.navigateTo({
      url:'/pages/login/login'
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})