import request from '../../utils/request'
// pages/login/login.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    phone: '',//手机号
    password:''//密码
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  //表单输入时处理函数
  handleInput (event) {
    // console.log(event.detail.value);
    // let type = event.currentTarget.id;//id形式传 取值：phone || password
    let type = event.currentTarget.dataset.type;//data-key 形式传值，在dataset里面取到
    // console.log(type);
    // 更新data中的数据
    this.setData({
      //对象属性是变量加中括号
      [type]:event.detail.value
    })
  },

  // 登录按钮处理函数
  async login () {
    // console.log(this.data.phone);
    // 收集表单数据
    let { phone, password } = this.data;
    /* 前端验证表单数据:
      1.手机号：
        1.1 不能为空
        1.2 格式是否正确
      2.密码：
        2.1 不能为空
        2.2 格式是否正确
    */
    // 验证手机号是否为空
    if (!phone) {
      // 手机号为空提示用户
      wx.showToast({
        title: '手机号不能为空',
        icon:'none'
      })
      return;
    }
    // 验证手机号格式
    let phoneRule = /^1(3[0-9]|4[01456879]|5[0-35-9]|6[2567]|7[0-8]|8[0-9]|9[0-35-9])\d{8}$/;
    if (!phoneRule.test(phone)) {
      // 手机号不正确提示用户
      wx.showToast({
        title: '手机号格式不正确',
        icon:'none'
      })
      return;
    }
    // 验证密码是否为空
    if (!password) {
      // 密码为空提示用户
      wx.showToast({
        title: '密码不能为空',
        icon:'none'
      })
      return;
    }

    // 验证密码格式是否正确
    /*1.密码必须由字母、数字组成，区分大小写;
      2.密码长度为6-18位 */
    let passwordRule = /^(?=.*[a-zA-Z])(?=.*[0-9])[A-Za-z0-9]{6,18}$/;
    if (!passwordRule.test(password)) {
      // 密码不符合要求提示用户
      wx.showToast({
        title: '密码必须由字母、数字组成，区分大小写，长度为6-18位',
        icon:'none'
      })
      return;
    }

    // // 前端验证通过
    // wx.showToast({
    //   title:'前端验证通过'
    // })

    // 后端验证,发送请求
    let result = await request('/login/cellphone', { phone, password,isLogin:true })
    if (result.code === 200) {
      wx.showToast({
        title:'登录成功'
      })

      // 将用户的信息存储至本地(同步)
      wx.setStorageSync('userInfo',JSON.stringify(result.profile))


      // 登录成功，跳转至tabBar个人中心
      // wx.switchTab({
      //   url:'/pages/personal/personal'
      // })
      wx.reLaunch({
        url:'/pages/personal/personal'
      })

    } else if (result.code === 501) {
      wx.showToast({
        title: '账号不存在',
        icon:'none'
      })
    }else if (result.code === 502) {
      wx.showToast({
        title: '密码错误',
        icon:'none'
      })
    }else {
      wx.showToast({
        title: '登录失败，请重新登录',
        icon:'none'
      })
    }


  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})