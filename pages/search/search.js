import request from '../../utils/request'
// 函数防抖使用
let isSend = null;
// pages/search/search.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 默认搜索关键词
    placeholderContent: '',
    // 热搜榜列表数据
    hotList: [],
    // 输入框输入的表单数据
    searchInputContent: '',
    // 关键字模糊搜索出来的数据
    searchList: [],
    // 搜索历史记录
    historyList:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 获取初始化数据
    this.getInitData();
    // 获取搜索历史记录
    this.getLocalSearchHistory();
  },
  // 获取初始化数据
  async getInitData () {
    // 获取默认搜索关键词
    let placeholderContentData = await request('/search/default');
    let placeholderContent = placeholderContentData.data.showKeyword;
    // 获取热搜榜列表数据
    let hotListData = await request('/search/hot/detail');
    let hotList = hotListData.data;
    this.setData({
      placeholderContent,
      hotList
    })
  },

/*******************************************************
  // 输入框表单数据发生改变的回调
  handleInputChange (event) {
    // console.log(event);
    // 更新输入框数据
    this.setData({
      searchInputContent:event.detail.value.trim()
    })
    if (isSend) {
      return;
    }
    isSend = true;
    this.getSearchList();
    
    // 函数节流
    setTimeout(() => {
      isSend = false;
    },300)
    
  },

 **************************************************************/

  // 输入框表单数据发生改变的回调，添加防抖
  handleInputChange (event) {
    if(isSend){
      clearTimeout(isSend) 
  }
    isSend = setTimeout(() => {
    // console.log(this.data);
    // console.log(event);
    // 更新输入框数据
    this.setData({
      searchInputContent:event.detail.value.trim()
    })
    // 获取搜索数据
    this.getSearchList();
  },300) 
  },

  // 获取搜索数据的功能函数
  async getSearchList () {
    // 输入框为空，不发请求直接返回
    if (!this.data.searchInputContent) {
      this.setData({
        searchList:[]
      })
      return;
    }
    let {searchInputContent,historyList } = this.data;
    // 发请求获取关键字模糊匹配数据
    let searchListData = await request('/search',{keywords:searchInputContent,limit:10})
    this.setData({
      searchList:searchListData.result.songs
    })
    // 检查之前是否有该关键词的搜索记录
    if (historyList.indexOf(searchInputContent) !== -1) {
      // 如果有，把该关键词在数组中去掉（即数组去重再在数组前面添加）
      historyList.splice(historyList.indexOf(searchInputContent),1)
    } 
    // 将搜索的关键字添加到搜索历史记录中
    historyList.unshift(searchInputContent);//在数组前面插入
    this.setData({
      historyList
    })
    // 将搜索历史记录保持到本地，这样小程序下次进来还有记录
    wx.setStorageSync('historyList',historyList)
  },

  // 获取本地历史记录功能函数
  getLocalSearchHistory () {
    let historyList = wx.getStorageSync('historyList');
    if (historyList) {
      this.setData({
        historyList
      })
    }
  },

  // 输入框叉号回调函数
  clearSearchInputContent () {
    this.setData({
      // 清空输入框搜索内容
      searchInputContent: '',
      // 清空模糊搜索列表，让热搜列表重新显示出来
      searchList: []
    })
  },

  // 搜索历史记录删除按钮回调
  deleteHistoryList () {
    // 弹出提示
    wx.showModal({
      title: '提示',
      content: '确认删除搜索记录？',
      success: (res)=> {
        if (res.confirm) {
          // console.log('用户点击确定')
          // 清空data中的historyList
          this.setData({
            historyList:[]
          })
          // 移除本地的历史记录缓存
          wx.removeStorageSync('historyList');
          // console.log(this);
        } 
      }
    })

    
  },

  // 防抖函数
  debounce(fn,delay) {
    let timer = null //借助闭包
    return function() {
        if(timer){
            clearTimeout(timer) 
        }
        timer = setTimeout(fn,delay) // 简化写法
    }
  },

  // 点击取消按钮的回调函数
  back () {
    wx.navigateBack({
      delta: 1
    })
  },

  // 点击搜索内容展示区歌曲的回调
  toSongDetail (event) {
    // console.log(event);
    // 获取当前歌曲信息
    let { musicid } = event.currentTarget.dataset;
    // console.log(musicid);
    
    // 导航,携带当前歌曲id
    wx.navigateTo({
      url:'/songPackage/pages/songDetail/songDetail?isSingle=YES&musicId=' + musicid
    })
  },

  // 点击历史记录时的回调
  historySearch (event) {
    // console.log(event.currentTarget.dataset.history);
    // 更新输入框数据
    this.setData({
      searchInputContent:event.currentTarget.dataset.history
    })
    // 获取搜索数据
    this.getSearchList();
  },

  // 输入框输入完成后点击键盘完成按钮时的回调
  handleConfirm () {
    // console.log('123');
    // 导航到模糊搜索歌曲列表页
    wx.navigateTo({
      url:'/pages/searchSongList/searchSongList'
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})