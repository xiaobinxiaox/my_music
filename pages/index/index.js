// 引入请求模块
import request from '../../utils/request'

// pages/index/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    bannerList: [],//轮播图数据
    recommendMusicList: [],//推荐歌单数据
    rankingList:[]//排行榜数据
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: async function (options) {
    // 发送请求
// wx.request({
//   url: 'http://localhost:3000/banner',
//   data: {type:2},
//   method: 'GET',
//   success: (res)=>{
//     console.log('请求成功：', res);
//   },
//   fail: (err) => {
//     console.log('请求失败：', err);
//   },
//   complete: ()=>{}
// });
    
    // 获取轮播图数据
    let bannerListData = await request('/banner', { type: 2 })
    // console.log(bannerListData);
    this.setData({
      bannerList:bannerListData.banners
    })

    // 获取推荐歌单数据
    let recommendMusicListData = await request('/personalized', { limit: 8 })
    this.setData({
      recommendMusicList:recommendMusicListData.result
    })


/********************************************************************    
    // 获取排行榜数据(内网穿透形式的api)
    
    //1. 排行榜有多种分类，根据idx划分，每一个idx代表一种排行榜
    //2. idx取值范围0-20，只取其中5种排行榜，取0-4
    //3. 每次请求发送一个idx，所以需要请求5次，获取5种排行榜数据
    
    let index = 0;
    let resultArr = [];
    while (index < 5) {
      // 取到idx对应排行榜的数据
      let rankingListData = await request('/top/list', { idx: index++ })
      // 对取到的数据进行提取
      let rankingListItem = {
        // 排行榜的名字
        name: rankingListData.playlist.name,
        // 排行榜里的歌曲数组，截取前3个
        tracks:rankingListData.playlist.tracks.slice(0,3)
      }
      // 循环体内把提取到的每种排行榜数据统一存放到一个数组里
      resultArr.push(rankingListItem)

      // 循环体内更新data中的排行榜数组数据，不需要等待5次请求全部结束才更新，用户体验较好，但是渲染次数较多
      this.setData({
        rankingList:resultArr
      })
    }
    // // 循环体外更新data中的排行榜数组数据，会导致发送请求过程中页面长时间白屏，用户体验较差
    // this.setData({
    //   rankingList:resultArr
    // })
******************************************************************************************** */   

    // 获取排行榜数据（vercel部署服务器的api）
    /*
    1. 先获所有种类的排行榜，然后提取前5种（0-4）排行榜名字、id
    2. 根据id获取每一种排行榜的歌单，提取前三首
    3. 每次请求发送一个id，所以需要请求5次，获取5种排行榜数据
    */
    // let index = 0;
    let resultArr = [];
    // 获取所有种类排行榜
    let topListData = await request('/toplist')
    
    for (let index = 0;index < 5;index++) {
      // 提取前5种排行榜的名字、id
      let topListItem = {
        // 排行榜的名字
        name: topListData.list[index].name,
        // 排行榜对应的id
        id:topListData.list[index].id
      }
      // 取到id对应排行榜歌曲数据
      let playlistData = await request('/playlist/detail',{id:topListItem.id})
      // 排行榜里的歌曲数组，截取前3个
      topListItem.tracks = playlistData.playlist.tracks.slice(0, 3)
      // 循环体内把提取到的每种排行榜数据统一存放到一个数组里
      resultArr.push(topListItem)
      // 循环体内更新data中的排行榜数组数据，不需要等待5次请求全部结束才更新，用户体验较好，但是渲染次数较多
      this.setData({
        rankingList:resultArr
      })
    }



  },

  // 跳转到每日推荐页面
  toRecommendSong () {
    wx.navigateTo({
      url:'/songPackage/pages/recommendSong/recommendSong'
    })
  },

  // 跳转到search页
  toSearch () {
    wx.navigateTo({
      url:'/pages/search/search'
    })
  },

  // 点击排行榜区歌曲的回调
  toSongDetail (event) {
    // console.log(event);
    // 获取当前歌曲信息
    let { musicid } = event.currentTarget.dataset;
    // console.log(musicid);
    
    // 导航,携带当前歌曲id
    wx.navigateTo({
      url:'/songPackage/pages/songDetail/songDetail?isSingle=YES&musicId=' + musicid
    })
  },





  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})