import request from '../../../utils/request'
import PubSub from 'pubsub-js'
import moment from 'moment'

// 获取全局实例(放在外面，即使页面销毁了，再次进来不用重新获取)
const appInstance = getApp();

// pages/songDetail/songDetail.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 标识音乐是否播放
    isPlay: false,
    // 歌曲详情对象
    song: {},
    // 音乐id
    musicId: '',
    // 音乐的链接
    musicLink: '',
    // 实时时长
    currentTime:'00:00',
    // 总时长
    durationTime: '03:00',
    // 实时进度条宽度
    currentWidth: 0,
    // 单曲or列表标志位
    isSingle:'NO'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // console.log(options);
    // 获取音乐id
    let musicId = options.musicId;
    // 获取单曲or列表标志位
    let isSingle = options.isSingle ? options.isSingle : 'NO';
    // console.log(isSingle);
    this.setData({
      musicId,
      isSingle
    })
    // console.log(musicId);
    // 获取音乐详情
    this.getMusicInfo(musicId);

    // 判断当前页面音乐是否在播放
    // 先判断是否有音乐在播放，再判断是否是当前音乐
    if (appInstance.globalData.isMusicPlay && appInstance.globalData.musicId === musicId) {
      // 修改当前页面音乐播放状态为true
      this.setData({
        isPlay:true
      })
    }

    // 创建控制音乐播放的实例，并绑定在页面实例上
    this.backgroundAudioManager = wx.getBackgroundAudioManager();
    /* （通过 控制音乐播放的实例 来）监听音乐的播放/暂停/停止，让
    用户通过系统播放/暂停按钮控制音乐时，详情页页面状态和真实音乐的状态一致。
    */
    // 监听播放
    this.backgroundAudioManager.onPlay(() => {
      // console.log('11');
      // 修改详情页音乐播放状态
      // this.setData({
      //   isPlay:true
      // })
      this.changePlayState(true);

      // 修改全局音乐播放状态（全局数据对象修改不用this.setData）
      // appInstance.globalData.isMusicPlay = true;
      appInstance.globalData.musicId = musicId;
    });
    // 监听暂停
    this.backgroundAudioManager.onPause(() => {
      // console.log('22');
      // 修改详情页音乐播放状态
      // this.setData({
      //   isPlay:false
      // })
      this.changePlayState(false);
      
    });
    // 监听停止
    this.backgroundAudioManager.onStop(() => {
      // console.log('33');
      // 修改详情页音乐播放状态
      // this.setData({
      //   isPlay:false
      // })
      this.changePlayState(false);
      
    });
    // 监听音乐实时播放进度
    this.backgroundAudioManager.onTimeUpdate(() => {
      // console.log('总时长,单位秒',this.backgroundAudioManager.duration);
      // console.log('实时时长,单位秒',this.backgroundAudioManager.currentTime);
      // moment格式要求是ms,所以要*1000
      let currentTime = moment(this.backgroundAudioManager.currentTime * 1000).format('mm:ss');
      let currentWidth = (this.backgroundAudioManager.currentTime / this.backgroundAudioManager.duration) * 450
      // console.log(currentWidth);
      this.setData({
        currentTime,
        currentWidth
      })

    });
    // 监听音乐自然播放结束
    this.backgroundAudioManager.onEnded(() => {

      if (this.data.isSingle === 'YES') {
        this.changePlayState(false);
        // 将实时进度条长度还原为0
        this.setData({
          currentTime:'00:00',
          currentWidth:0
        })
        return;
      }

      // 订阅来自recommendSong页面发布的musicId消息
      PubSub.subscribe('musicId', (msg,musicId) => {
        // console.log(musicId);

        // 获取音乐详情
        this.getMusicInfo(musicId);
        // 自动播放当前音乐
        this.musicControl(true, musicId);

        // 取消订阅(每次订阅完之后删除该订阅，因为每次执行消息池会产生累加)
        PubSub.unsubscribe('musicId');
      })
      
      // console.log('123131312');
      // 自动切换到下一首歌并且自动播放
      PubSub.publish('switchType','next')
      // 将实时进度条长度还原为0
      this.setData({
        currentTime:'00:00',
        currentWidth:0
      })

    })

    

    

  },

  // 修改音乐播放状态的函数
  changePlayState (isPlay) {
    this.setData({
      isPlay
    })
    // 修改全局音乐播放状态（全局数据对象修改不用this.setData）
    appInstance.globalData.isMusicPlay = isPlay;
  },

  // 点击播放/暂停回调函数
  handleMusicPlay () {
    let isPlay = !this.data.isPlay;
    // this.setData({
    //   isPlay
    // })
    let {musicId,musicLink} = this.data
    this.musicControl(isPlay,musicId,musicLink);
  },

  // 获取音乐详情函数
  async getMusicInfo (musicId) {
    let songData = await request('/song/detail', { ids: musicId });
    // console.log(songData);
    // 总时长获取与转换(songData.songs[0].dt单位是ms)
    let durationTime = moment(songData.songs[0].dt).format('mm:ss')

    this.setData({
      song: songData.songs[0],
      durationTime
    })
    // 动态设置当前窗口的标题
    wx.setNavigationBarTitle({
      title:songData.songs[0].name
    });
  },

  // 控制音乐播放/暂停的功能函数
  async musicControl (isPlay, musicId,musicLink) {

    if (isPlay) {
      // 播放音乐

      if (!musicLink) {
        // 获取音乐播放链接
        // let musicLinkData = await request('/song/url', { id: this.data.song.id });
        let musicLinkData = await request('/song/url', { id: musicId});
        musicLink = musicLinkData.data[0].url;
        this.setData({
          musicLink
        })
      }

      
      // 给控制实例添加播放链接
      this.backgroundAudioManager.src = musicLink;
      this.backgroundAudioManager.title = this.data.song.name ;
    } else {
      // 暂停音乐
      this.backgroundAudioManager.pause();
    }
  },

  // 点击切换歌曲的处理函数（上一首/下一首）
  handleSwitchMusic (event) {
    // 获取切歌的类型
    let type = event.currentTarget.id;
    // console.log(type);

    // 判断单曲or列表
    if (this.data.isSingle === 'YES') {
      let title = type === 'pre' ? '没有上一首歌曲' : '没有下一首歌曲';
      wx.showToast({
        title,
        icon: 'none',
        duration: 2000
      })
      return;
    }

    // 关闭当前音乐
    this.backgroundAudioManager.stop();

    // 订阅来自recommendSong页面发布的musicId消息
    PubSub.subscribe('musicId', (msg,musicId) => {
      // console.log(musicId);

      // 获取音乐详情
      this.getMusicInfo(musicId);
      // 自动播放当前音乐
      this.musicControl(true, musicId);

      // 取消订阅(每次订阅完之后删除该订阅，因为每次执行消息池会产生累加)
      PubSub.unsubscribe('musicId');
    })

    // 发布 消息数据给recommendSong页面(提供切歌的类型)
    PubSub.publish('switchType',type)

  },
  

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})