import request from '../../../utils/request'
import PubSub from 'pubsub-js'

// pages/recommendSong/recommendSong.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 年
    year:'',
    // 月
    month:'',
    // 日
    day: '',
    // 星期
    week: '',
    // 推荐歌曲列表
    recommendList: [],
    // 点击音乐的下标
    index:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 判断用户是否登录
    let userInfo = wx.getStorageSync('userInfo');
    if (!userInfo) {
      // 如果未登录
      // 弹出提示
      wx.showToast({
        title: '请登录',
        icon: 'none',
        success: () => {
          // 跳转到登录界面
          wx.reLaunch({
            url:'/pages/login/login'
          })
        }
      })
    }

    // 获取星期
    let week = this.handleWeekNumChange();
    // 更新日期数据
    this.setData({
      // 年
      year: new Date().getFullYear(),
      // 月,月份要+1
      month: new Date().getMonth() + 1,
      // 日
      day: new Date().getDate(),
      // 星期
      week
    })

    // 获取每日推荐歌曲
    this.getRecommendSongList();

    // 订阅来自songDetail页面发布的消息（切换上一首/下一首）
    PubSub.subscribe('switchType', (msg, type) => {
      let { recommendList,index } = this.data;
      // console.log(msg,type);
      // msg 事件名 ；type  传来的消息，即切歌的类型
      if (type === 'pre') {
        // 上一首
        // 如果是第一首则跳转到最后一首
        (index === 0) && (index = recommendList.length);
        index -= 1;
      } else {
        // 下一首
        // 如果是最后一首则跳转到第一首
        (index === recommendList.length - 1) && (index = -1);
        index += 1;
      }
      // 更新下标
      this.setData({
        index
      })

      // console.log(recommendList);
      // 上一首/下一首的id
      let musicId = recommendList[index].id;
      // console.log(musicId);
      // 将音乐id回传给songDetail页面
      PubSub.publish('musicId', musicId)
    });
  },

  // 星期的数字转换汉字
  handleWeekNumChange () {
    var week;
    switch (new Date().getDay()) {
        case 0:
          week = "星期日";
            break;
        case 1:
          week = "星期一";
            break;
        case 2:
          week = "星期二";
            break;
        case 3:
          week = "星期三";
            break;
        case 4:
          week = "星期四";
            break;
        case 5:
          week = "星期五";
            break;
        case 6:
          week = "星期六";
            break;
    }
    return week;
  },

  // 获取每日推荐歌曲
  async getRecommendSongList () {
    let recommendListData = await request('/recommend/songs');
    this.setData({
      recommendList:recommendListData.data.dailySongs
    })
  },

  // 跳转至歌曲详情(songDetail)页面
  toSongDetail (event) {
    // 获取当前歌曲信息
    // let song = event.currentTarget.dataset.song;
    // let index = event.currentTarget.dataset.index;
    let { song, index } = event.currentTarget.dataset;
    
    this.setData({
      index
    })
    // console.log(song);
    // 导航,携带当前歌曲id
    wx.navigateTo({
      url:'/songPackage/pages/songDetail/songDetail?musicId=' + song.id
    })
  },


  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})