// 引入服务器配置对象
import config from './config'

// 发送ajax请求
// 部分形参先预设默认值
export default (url, data = {}, method = 'GET') => {
  // 异步任务，设置promise返回结果
  return new Promise((resolve,reject) => {
    wx.request({
      // 本地
      // url: config.host + url,
      // vercel部署
      url: config.vercelHost + url,
      // 内网穿透
      // url: config.mobileHost + url,
      data,
      method,
      // 设置请求头携带cookie
      header: {
        /*
        1.先判断本地存储是否有cookies数组，没有则赋值空
        2.如果有，则读取数组，找出需要的cookie返回
        */
        cookie:wx.getStorageSync('cookies') ? wx.getStorageSync('cookies').find(item=>item.indexOf('MUSIC_U') !== -1) : ''
      },
      success: (res) => {

        // console.log(res);
        // 先判断是否为登录请求
        if (data.isLogin) {
          // 将用户的cookies数组存入本地
          wx.setStorage({
            key: 'cookies',
            data:res.cookies
          })
          console.log(res);
        }

        // console.log('请求成功：', res);
        // 过滤出需要的数据
        resolve(res.data)
      },
      fail: (err) => {
        // console.log('请求失败：', err);
        reject(err)
      },
      complete: ()=>{}
    })
  })
}