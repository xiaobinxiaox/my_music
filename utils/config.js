// 配置服务器相关信息
export default {
  // 本地
  host: 'http://localhost:3000',
  // 内网穿透
  // mobileHost: 'http://xiaobin.free.idcfengye.com',
  mobileHost:'http://music163.free.idcfengye.com',
  // vercel部署
  vercelHost:'https://netease-cloud-music-api-elwhguxoq-xiaobinxiaoxiong.vercel.app'
}